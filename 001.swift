let cloneFromUrlMenuItemId = "Clone from URL"
    let repoTypeLabelGitId = "This is a Git repository"
    let repoTypeLabelHgId = "This is a Mercurial repository"
    let cloneButtonId = "Clone"



func testClone()
    {
        let repobrowserWindow = app.windows.matching(identifier: constants.repobrowserWindowId)
        //Validate that main app window visible
        XCTAssertEqual(repobrowserWindow.count, 1)
        
        let newButton = repobrowserWindow.menuButtons[constants.newToolbarButtonId]
        //Verify that New button exists and Enabled
        XCTAssertTrue(newButton.isEnabled)
        newButton.click()
        
        repobrowserWindow.menuItems[constants.cloneFromUrlMenuItemId].click()
        
        let cloneQuery = repobrowserWindow.sheets
        
        //Validate text labels on Clone pop-up window
        XCTAssert(cloneQuery.staticTexts["Clone a repository"].exists)
        XCTAssert(cloneQuery.staticTexts["This is not a valid source path / URL"].exists)
        
        //Set URL for cloning
        let editUrlField = cloneQuery.children(matching: .textField).element(boundBy: 0)
        XCTAssert(editUrlField.exists)
        editUrlField.click()
        editUrlField.typeText(Constants.bbcPublicGitUrlHttps)
        
        //Set path folder
        let editPathField = cloneQuery.children(matching: .textField).element(boundBy: 1)
        XCTAssert(editPathField.exists)
        editPathField.click()
        editPathField.typeKey(XCUIKeyboardKeyDownArrow , modifierFlags:[])
        editPathField.typeKey(XCUIKeyboardKeyDelete, modifierFlags: .command)
        
        let testFolder = getDocumentsDirectory()+"/Test1"
        editPathField.typeText(testFolder)
        
        //Wait for repository type label refreshed
        let exists = NSPredicate(format: "exists == 1")
        expectation(for: exists, evaluatedWith: repoTypeGit, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
        
        //Verify that displayed correct label for repository type
        XCTAssert(repoTypeGit.exists)
        
        //Perform clone repository action
        print("Cloning test repository")
        cloneQuery.buttons[constants.cloneButtonId].click()
        
        //Wait while repository cloning
        let repoFolder = app.windows["repo:"+testFolder]
        expectation(for: exists, evaluatedWith: repoFolder, handler: nil)
        waitForExpectations(timeout: 10, handler: nil)
        
        XCTAssert(repoFolder.staticTexts["Test1 (Git)"].exists)
        repoFolder.staticTexts["History"].click()
        repoFolder.tables.containing(.tableColumn, identifier:"graph").children(matching: .tableRow).element(boundBy: 0).staticTexts["Visual Graph"].click()
        
        //Verifying that cloned repository folder created
        XCTAssert(FileManager.default.fileExists(atPath: testFolder))
        
        repoFolder.buttons[XCUIIdentifierCloseWindow].click()
        
        //Deleting cloned repository folder
        deleteFolder(path: testFolder)
        
        repobrowserWindow.outlines.staticTexts["Test1"].click()
        repobrowserWindow.outlines.containing(.tableColumn, identifier:"AutomaticTableColumnIdentifier.0").element.typeKey(XCUIKeyboardKeyDelete, modifierFlags:[])
        app.dialogs["alert"].typeText("\r")
        
    }